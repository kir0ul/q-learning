help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

jupytext: ## Convert JSON notebook to plain text
	jupytext --to py:nomarker **/*.ipynb

format: ## Autoformat files
	make jupytext
	black .

lint: ## Lint files
	ruff .
